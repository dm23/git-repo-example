package com.citi.training.git;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitRepoExampleApplication 
{
	public static void main(String[] args) 
	{
		SpringApplication.run(GitRepoExampleApplication.class, args);
	}
}